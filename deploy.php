<?php
namespace Deployer;

require 'recipe/common.php';

// Project name
set('application', 'Pegasus API Platform');

// Project repository
set('repository', 'git@bitbucket.org:contemivn/pegasus-api-platform.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true); 
set('cleanup_use_sudo', true);
set('writable_use_sudo', true);

// Shared files/dirs between deploys 
set('shared_files', []);
set('shared_dirs', ['web/assets/image']);

// Writable dirs by web server 
set('writable_dirs', ['var', 'bin', 'web', 'vendor', 'web/assets/image']);
set('allow_anonymous_stats', false);
set('http_user', 'ubuntu');
set('composer_action', 'install');
set('composer_options', '{{composer_action}} --no-dev --ignore-platform-reqs -vvv');
set('default_timeout', 1000);
set('env', [
    'SYMFONY_ENV' => 'prod',
    'API_PLATFORM_WEB_PORT' => 80,
    'API_PLATFORM_VARNISH_PORT' => 99,
    'API_PLATFORM_PHPMYADMIN_PORT' => 81
]);

// Hosts
inventory('deploy.yml');

// Tasks
task('deploy:make-env', function () {
    run("cd {{release_path}} && cp -rf patch/* .");
});

task('deploy:patch', function () {
    run("cd {{release_path}} && cp .env.dist .env");
});

task('deploy:docker-compose', function () {
    run('cd {{release_path}} && docker rm -f apiplatform-varnish apiplatform-nginx apiplatform-app apiplatform-logio apiplatform-harvester');
    run('cd {{release_path}} && docker-compose up --force-recreate -d');
});

desc('Deploy Pegasus API Platform');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:make-env',
    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:docker-compose',
    'deploy:unlock',
    'cleanup',
    'success'
]);

// Hooks
before('deploy:docker-compose', 'deploy:make-env');
after('deploy:failed', 'deploy:unlock');
after('deploy:vendors', 'deploy:patch');

